package com.qt.myboot.dao;

import com.qt.myboot.entity.User;
import com.qt.myboot.entity.UserRole;
import com.qt.myboot.entity.UserRoleRelation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRelationRepository extends BaseRepository<UserRoleRelation, Integer> {

    @Query(value = "SELECT\n" +
            "\tb.role\n" +
            "FROM\n" +
            "\tboot_user_role_relation a\n" +
            "INNER JOIN boot_role b ON a.role_id = b.id\n" +
            "WHERE\n" +
            "\ta.user_id = ?1", nativeQuery = true)
    List<String> findRoleByUserId(Integer userId);


    @Query(value = "SELECT\n" +
            "\tb.id\n" +
            "FROM\n" +
            "\tboot_user_role_relation a\n" +
            "INNER JOIN boot_role b ON a.role_id = b.id\n" +
            "WHERE\n" +
            "\ta.user_id = ?1", nativeQuery = true)
    List<Integer> findRoleIdByUserId(Integer userId);

}
