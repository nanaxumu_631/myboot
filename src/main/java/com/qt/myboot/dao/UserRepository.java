package com.qt.myboot.dao;

import com.qt.myboot.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User, Integer> {

    User findByUserNameEquals(String userName);

    User findByUserNameEqualsAndPasswordEquals(String name, String password);
}
