package com.qt.myboot.enums;

import com.qt.myboot.interfaces.ErrorCode;


public enum UserErrorCodeEnum implements ErrorCode {

    US00("1000", "1000"),
    US01("1001", "用户已存在"),
    US02("1002", "用户尚未注册"),
    US03("1003", "链接已过期"),
    US04("1004", "请先激活帐号"),
    US05("1005", "帐号已冻结"),
    US06("1006", "帐号已冻结并不能解冻"),
    US07("1007", "冻结审核"),
    US08("1101", "密码不符合规则"),
    US09("1102", "手机格式不正确"),
    US10("1112", "手机号已存在"),
    US11("1103", "email格式不正确"),
    US12("1104", "日期格式不正确"),
    US13("1105", "输入字符太长"),
    US14("1106", "输入不能为空"),
    US15("1107", "url格式错误"),
    US16("1108", "两次输入的密码不同"),
    US17("1109", "新旧密码不能相同"),
    US18("1110", "原密码错误"),
    US19("1111", "用户名或密码错误"),
    US20("1201", "用户已存在"),
    US21("1202", "用户尚未注册"),
    US22("1203", "已通过认证"),
    US23("1204", "请先完成认证"),
    US24("1206", "请等待验证"),
    US25("1207", "邀请码错误"),
    US26("2001", "验证码类型错误"),
    US27("2002", "验证格式不正确"),
    US28("2003", "验证码错误"),
    US29("2004", "验证码发送过于频繁"),
    US30("2005", "请先绑定谷歌验证"),
    US31("2007", "谷歌秘钥错误"),
    US32("2006", "请先绑定手机号"),
    US33("3001", "冻结失败"),
    US34("5001", "服务器错误"),
    US35("3002", "token已过期"),
    US36("3003", "60秒后再重试"),
    US37("3004", "验证码错误超过3次"),
    US39("2008", "请先绑定邮箱"),
    US40("1208", "二次校验至少开启一个"),
    US41("3006", "操作token过期"),
    US42("3007", "api异常"),
    US43("1112", "请先设置支付密码"),
    US44("1113", "支付密码不正确"),
    US45("1114","证件号已上传"),
    US46("1115", "权限不足,请联系管理员"),
    US47("1116", "令牌不正确或者已过期"),
    ;

    /**
     * 错误代码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;

    UserErrorCodeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public void setDesc(String desc) {
        this.desc = desc;
    }
}
