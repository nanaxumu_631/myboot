package com.qt.myboot.config;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Despription: AQS 是java JUC包中重要的类 用于构建锁和同步器
 *
 * @Autor: xusl
 * @Date: 2019/6/25 15:12
 * @Version: 1.0
 **/
public class LockImpl extends AbstractQueuedSynchronizer {
    /**
     Synchronized 和 ReenTrantLock 的对比
     1.两者都是可重入锁

     两者都是可重入锁。“可重入锁”概念是：自己可以再次获取自己的内部锁。比如一个线程获得了某个对象的锁，此时这个对象锁还没有释放，当其再次想要获取这个对象的锁的时候还是可以获取的，如果不可锁重入的话，就会造成死锁。同一个线程每次获取锁，锁的计数器都自增1，所以要等到锁的计数器下降为0时才能释放锁。

     2.synchronized 依赖于 JVM 而 ReenTrantLock 依赖于 API

     synchronized 是依赖于 JVM 实现的，前面我们也讲到了 虚拟机团队在 JDK1.6 为 synchronized 关键字进行了很多优化，但是这些优化都是在虚拟机层面实现的，并没有直接暴露给我们。ReenTrantLock 是 JDK 层面实现的（也就是 API 层面，需要 lock() 和 unlock 方法配合 try/finally 语句块来完成），所以我们可以通过查看它的源代码，来看它是如何实现的。

     3.ReenTrantLock 比 synchronized 增加了一些高级功能

     相比synchronized，ReenTrantLock增加了一些高级功能。主要来说主要有三点：①等待可中断；②可实现公平锁；③可实现选择性通知（锁可以绑定多个条件）

     ReenTrantLock提供了一种能够中断等待锁的线程的机制，通过lock.lockInterruptibly()来实现这个机制。也就是说正在等待的线程可以选择放弃等待，改为处理其他事情。
     ReenTrantLock可以指定是公平锁还是非公平锁。而synchronized只能是非公平锁。所谓的公平锁就是先等待的线程先获得锁。 ReenTrantLock默认情况是非公平的，可以通过 ReenTrantLock类的ReentrantLock(boolean fair)构造方法来制定是否是公平的。
     synchronized关键字与wait()和notify/notifyAll()方法相结合可以实现等待/通知机制，ReentrantLock类当然也可以实现，但是需要借助于Condition接口与newCondition() 方法。Condition是JDK1.5之后才有的，它具有很好的灵活性，比如可以实现多路通知功能也就是在一个Lock对象中可以创建多个Condition实例（即对象监视器），线程对象可以注册在指定的Condition中，从而可以有选择性的进行线程通知，在调度线程上更加灵活。 在使用notify/notifyAll()方法进行通知时，被通知的线程是由 JVM 选择的，用ReentrantLock类结合Condition实例可以实现“选择性通知” ，这个功能非常重要，而且是Condition接口默认提供的。而synchronized关键字就相当于整个Lock对象中只有一个Condition实例，所有的线程都注册在它一个身上。如果执行notifyAll()方法的话就会通知所有处于等待状态的线程这样会造成很大的效率问题，而Condition实例的signalAll()方法 只会唤醒注册在该Condition实例中的所有等待线程。
     如果你想使用上述功能，那么选择ReenTrantLock是一个不错的选择
     **/

     Lock lock = new ReentrantLock();


     /**condition结合reentranlock可实现选择性通知**/
//     Condition condition = new Condition() {
//         @Override
//         public void await() throws InterruptedException {
//         }
//
//         @Override
//         public void awaitUninterruptibly() {
//             Executors.newCachedThreadPool();
//
//         }
//
//         @Override
//         public long awaitNanos(long nanosTimeout) throws InterruptedException {
//             return 0;
//         }
//
//         @Override
//         public boolean await(long time, TimeUnit unit) throws InterruptedException {
//             return false;
//         }
//
//         @Override
//         public boolean awaitUntil(Date deadline) throws InterruptedException {
//             return false;
//         }
//
//         @Override
//         public void signal() {
//         }
//
//         @Override
//         public void signalAll() {
//
//         }
//     };

    private static final long serialVersionUID = -7241681696598904611L;

    private volatile int status;

    @Override
    protected boolean tryAcquire(int arg) {
        return super.tryAcquire(arg);
    }

    @Override
    protected boolean tryRelease(int arg) {
        return super.tryRelease(arg);
    }

    @Override
    protected int tryAcquireShared(int arg) {
        return super.tryAcquireShared(arg);
    }

    @Override
    protected boolean tryReleaseShared(int arg) {
        return super.tryReleaseShared(arg);
    }

    @Override
    protected boolean isHeldExclusively() {
        return super.isHeldExclusively();
    }
}
