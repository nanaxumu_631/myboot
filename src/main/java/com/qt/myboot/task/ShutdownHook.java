package com.qt.myboot.task;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Despription: 添加一个钩子  非正常关闭不会触发钩子，当系统执行了system.exit()等程序正常退出都会执行；
 *
 * @Autor: xusl
 * @Date: 2019/6/17 17:30
 * @Version: 1.0
 **/
@Component
public class ShutdownHook {

    @PostConstruct
    public void shutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("执行钩子计划");
        }));
    }


}
