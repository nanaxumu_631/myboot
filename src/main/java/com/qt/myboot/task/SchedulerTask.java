package com.qt.myboot.task;

import org.joda.time.DateTime;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@EnableScheduling
public class SchedulerTask {
 
 
    @Scheduled(fixedDelay = 20000) //或者cron="*/2 * * * * ?"
    @Async(value = "asyncTaskExecutor")
    public void fixedDelayJob() throws InterruptedException {
        String name = Thread.currentThread().getName();
        Thread.sleep(5000);
        System.out.println("当前线程："+ name +  "    2s一次"+ DateTime.now().toDate());
    }
}
