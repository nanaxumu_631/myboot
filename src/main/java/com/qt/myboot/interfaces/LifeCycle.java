package com.qt.myboot.interfaces;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Despription:
 *
 * @Autor: xusl
 * @Date: 2019/6/28 11:57
 * @Version: 1.0
 **/
public class LifeCycle implements InitializingBean, DisposableBean {
    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}
