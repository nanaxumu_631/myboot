package com.qt.myboot.interfaces;

public interface ResultStatus {

     Integer getCode();

     String getMsg();

}
