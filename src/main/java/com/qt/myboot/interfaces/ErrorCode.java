package com.qt.myboot.interfaces;

/**
 * @author: fatey
 * @date: 2018/3/1
 * @time: 15:04
 * Description:
 */
public interface ErrorCode {

    String getDesc();

    void setDesc(String desc);

    String getCode();

    void setCode(String code);
}
