package com.qt.myboot.service;

import com.qt.myboot.common.BootConstants;
import com.qt.myboot.dao.UserRepository;
import com.qt.myboot.entity.User;
import com.qt.myboot.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void addUser(User user) {
        user.setPassword(MD5Util.encode(user.getPassword(), BootConstants.SALT));
        userRepository.save(user);
    }

    public User findUser(User user) {
        return userRepository.findByUserNameEqualsAndPasswordEquals(user.getUserName(), MD5Util.encode(user.getPassword(), BootConstants.SALT));
    }

    public User findByName(User user) {
        return userRepository.findByUserNameEquals(user.getUserName());
    }

    public List<User> findAllUser() {
        return userRepository.findAll();
    }
}
