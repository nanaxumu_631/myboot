package com.qt.myboot.service;

import com.qt.myboot.entity.Product;

/**
 * Despription:
 *
 * @Autor: xusl
 * @Date: 2019/6/26 11:53
 * @Version: 1.0
 **/
public interface ProductService {

     void add(Product product) throws Exception;

     void update(Product product) throws Exception;

     Product findOne(Integer id) throws Exception;
}
