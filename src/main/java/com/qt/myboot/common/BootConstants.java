package com.qt.myboot.common;

public class BootConstants {

    public final static String bas64 = "TmtsdGVUWkpiWGsxTlhGRlpXNXdhamMzZVUwMlNXMTVOa2x0ZVRVMWNVVTFZa05RTlZwSGJ6VmFSMjgzTjNsTk5rbHRlVFpKYlhrMU5YRkZaVWhrTURjM2VVMDFObWw2TlRWeFJUVnlZV0kxV2s5c056ZDVUVFZpUTFBMlNTdElOVmxsU2pWWkt6STFZVEpSTnpkNVRUVmhVMjQyV1VkVU5UVkRSelZ5VnpNMVdrOXM=";

    /**
     * jwt iss 签名
     */
    public final static String SIGNATURE = "mqqIad.xu";

    /**
     * jwt aud 接收着
     */
    public final static String jwtAud = "bootAudience";

    /**
     * token 过期时间 3小时
     */
    public final static long tokenTime = 10800000;

    /**
     * token 小于 一小时生成新Token
     */
    public final static long expire = 10800000;

    /**
     * MD5加密盐
     */
    public static final String SALT = "boot";
}
