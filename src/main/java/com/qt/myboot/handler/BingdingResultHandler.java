package com.qt.myboot.handler;

import com.qt.myboot.common.Result;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

public class BingdingResultHandler {

    public static Result handler(BindingResult bingdingresult) {
        if (bingdingresult.hasErrors()) {
            List list = new ArrayList();
            bingdingresult.getAllErrors().forEach((objectError) -> {
                list.add(objectError.getDefaultMessage());
            });
            return Result.failed(list);
        } else {
            return Result.success();
        }
    }
}
